
package fatihdemirag.net.dersprogram.DbHelpers;
/**
 * Created by fxd on 10.06.2017.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.Calendar;

public class DbHelper extends SQLiteOpenHelper{
    public static final String dbName="ders_programi";
    private static final String table = "dersler_programi";
    private static final int version = 9;
    public static final String col_1="ders_adi";
    public static final String col_2="ders_gunu";
    public static final String col_3="ders_baslangic_saati";
    public static final String col_4="ders_bitis_saati";
    public static final String col_5 = "ders_pozisyon";
    public static final String col_6 = "ders_tenefus_suresi";

    private static final String table_2 = "ders_notlari";
    public static final String col_1_2="ders_konusu";
    public static final String col_2_2="ders_notu";
    public static final String col_3_2="not_resmi";
    public static final String col_4_2="ders";

    private static final String table_3 = "dersler";
    public static final String col_1_3 = "ders_adi";

    SQLiteDatabase db;

    long result;
    long result2;
    long result3;

    private void TostMesaj(String tost)
    {
        Toast.makeText(null,tost,Toast.LENGTH_SHORT).show();
    }
    public DbHelper(Context context)
    {
        super(context,dbName,null,version);
        db=this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL("create table " + table + " (id integer primary key autoincrement," + col_1 + " text not null," + col_2 + " text," + col_3 + " text," + col_4 + " text," + col_5 + " int," + col_6 + " text)");
            db.execSQL("create table "+table_2+" (id integer primary key autoincrement,"+col_1_2+" text not null,"+col_2_2+" text not null,"+col_3_2+" blob,"+col_4_2+" text)");
            db.execSQL("create table " + table_3 + " (id integer primary key autoincrement," + col_1_3 + " text not null unique)");

        }catch (Exception e)
        {
            try {
                TostMesaj("Hata Oluştu");
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    public boolean dersEkle(String dersAdi, String ders_gunu, String dersBaslangicSaati, String dersBitisSaati, int dersPoziyon, String dersTenefusSuresi)
    {
        db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(col_1, dersAdi);
        contentValues.put(col_2, ders_gunu);
        contentValues.put(col_3, dersBaslangicSaati);
        contentValues.put(col_4, dersBitisSaati);
        contentValues.put(col_5, dersPoziyon);
        contentValues.put(col_6, dersTenefusSuresi);
        result = db.insert(table, null, contentValues);
        if (result == 0)
            return false;
        else
            return true;

    }

    public Cursor tumDersler()
    {
        db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from dersler_programi ", null);
        return cursor;
    }

    public Cursor dersler()
    {
        db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select distinct ders_adi from dersler", null);
        return cursor;
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists "+table);
        db.execSQL("drop table if exists "+table_2);
        db.execSQL("drop table if exists " + table_3);
        onCreate(db);
    }

    public Integer dersSil(String id)
    {
        db = this.getWritableDatabase();
        return db.delete(table,"id = ?",new String[] {id});
    }

    public boolean dersGuncelle(int dersId, String dersAdi, int dersPozisyon, String dersTenefusSuresi) {
        db = this.getWritableDatabase();
        String updateQuery = "update '" + table + "' set '" + col_1 + "'='" + dersAdi + "','" + col_5 + "'='" + dersPozisyon + "','" + col_6 + "'='" + dersTenefusSuresi + "' where id='" + dersId + "'";
        db.execSQL(updateQuery);
        return true;
    }

    //--------Tablo 2---------//

    public boolean dersNotuGuncelle(int id, String not) {
        db = this.getWritableDatabase();
        String updateQuery = "update '" + table_2 + "' set '" + col_2_2 + "'='" + not + "' where id='" + id + "'";
        db.execSQL(updateQuery);
        return true;
    }

    public boolean dersNotuEkle(String konu, String ders, byte[] notResmi, String dersNotu)
    {

        db = this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(col_1_2,konu);
        contentValues.put(col_2_2,dersNotu);
        contentValues.put(col_3_2,notResmi);
        contentValues.put(col_4_2,ders);
        result2=db.insert(table_2,null,contentValues);
        if (result2 == 0)
            return false;
        else
            return true;
    }

    public Cursor dersNotlari(String ders)
    {
        db = this.getWritableDatabase();
        String sql="select * from ders_notlari where ders=?";
        Cursor cursor=db.rawQuery(sql,new String[]{ders});
        return cursor;
    }

    public Cursor ResimBul(String id)
    {
        db = this.getWritableDatabase();
        String sql="select not_resmi from ders_notlari where id=?";
        Cursor cursor=db.rawQuery(sql,new String[]{id});
        return cursor;
    }
    public void NotSil(String ders)
    {
        db = this.getWritableDatabase();
        String query="delete from '"+table_2+"' where ders='"+ders+"'";
        db.execSQL(query);
        db.close();

    }

    public void NotSilTekli(int id) {
        db = this.getWritableDatabase();
        String query = "delete from '" + table_2 + "' where id='" + id + "'";
        db.execSQL(query);
        db.close();

    }

    //  ---------------------Dersler Tablosu----------------------------------

    public Integer dersSilTekli(String ders) {
        db = this.getWritableDatabase();
        return db.delete(table_3, "ders_adi = ?", new String[]{ders});
    }


    public boolean dersEkle(String dersAdi) {
        db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(col_1_3, dersAdi);
        result3 = db.insert(table_3, null, contentValues);
        if (result3 == 0)
            return false;
        else
            return true;

    }

//    ---------------------------------------------------
private String gun = "";
    public Cursor dersKontrol(String saat) {
        switch (Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1) {
            case 0:
                gun = "Pazar";
                break;
            case 1:
                gun = "Pazartesi";
                break;
            case 2:
                gun = "Salı";
                break;
            case 3:
                gun = "Çarşamba";
                break;
            case 4:
                gun = "Perşembe";
                break;
            case 5:
                gun = "Cuma";
                break;
            case 6:
                gun = "Cumartesi";
                break;
        }
        db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from dersler_programi where ders_baslangic_saati='" + saat + "' and ders_gunu='" + gun + "'", null);
        return cursor;
    }

    public void dersProgramiSil() {
        db = this.getWritableDatabase();
        String query = "drop table dersler_programi";
        db.execSQL(query);
        db.close();
    }


}
